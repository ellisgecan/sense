//TODO convert get to post if data given -- need error checking too?

//TODO update to allow get args
//TODO update to work with url.parse
//TODO 
// localhost:9200/method+shit getopts -d dataJSON

//TODO grammar for _bulk
//TODO implement readable/writable stream api

// callbacks are passed in



var http = require('http');
var url = require('url');
module.exports = clientFactory;
return;
function clientFactory(hostname, port) {
  var construct = {};
  construct.get = senseRequest.bind(construct,'GET');
  construct.put = senseRequest.bind(construct,'PUT');
  construct.head = senseRequest.bind(construct,'HEAD');
  construct.post = senseRequest.bind(construct,'POST');
  construct.delete = senseRequest.bind(construct,'DELETE');
  
  if(!port) {
    if(/:/.test(hostname)) {
      var split = hostname.split(':');
      hostname = split[0];
      port = split[1];
    } else {
      port = 80;
    }
  }
  
  return construct;
  
  
  function senseRequest(method,path,data,callback) {
    var get,post; //TODO
    if(data) {
      if(data.constructor===Function) {
        callback = data;
        data = null;
      } else {
        if(method==='GET') {
          method = 'POST';
        }
      }
    }
    var opts = {
      hostname: hostname,
      method: method,
      port: port,
      path: path
    };
    
    var chunks = [];
    var req = http.request(opts,function(res) {
      res.on('data',function(chunk) {
        chunks.push(chunk.toString('utf8'));
      });
      res.on('end',function() {
        if(callback) {
          callback(chunks.join(''));
        }
      });
    });
    
    if(data) {
      if(data.constructor!==String) {
        data = JSON.stringify(data);
      }
      //req.write("'");
      req.write(data);
      //req.write("'");
    }
    
    req.end();
  }
}

